const createError = require("http-errors");
const express = require("express");
const cookieParser = require("cookie-parser");
const logger = require("morgan");


const loginRouter = require("./routes/login");
const articlesRouter = require("./routes/article");
const scriptRouter = require("./routes/script");
const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());

app.use("/login", loginRouter);
app.use("/articles", articlesRouter);
app.use("/script", scriptRouter);

app.use((req, res, next) => {
   next(createError(404));
});

module.exports = app;