const jwt = require("jsonwebtoken");
const createError = require("http-errors");
const Promise = require("bluebird");
const jwtVerifyAsync = Promise.promisify(jwt.verify, jwt);
const secret = process.env.JWT_PRIVATE_KEY || "123456789";


module.exports = {
  verifyToken: (req, res, next) => {
      const bearerHeader = req.headers["authorization"];
      if (typeof bearerHeader !== "undefined") {
          const bearer = bearerHeader.split(" ");
          req.token = bearer[1];
          next();
      } else {
          res.sendStatus(403);
      }
  },
    checkAuth: async (req, res, next) => {
        try {
            if (req.headers.authorization) {
                const token = req.token;
                const authData = await jwtVerifyAsync(token, secret);
                req.authData = authData;
                next();
            } else {
                next(createError(404));
            }
        } catch (e) {
            next(createError(403));
        }
    },
    idAdmin: (req, res, next) => {
      console.log(req.authData.infoAccount.role);
      if (req.authData.infoAccount.role === 0 || typeof req.authData.infoAccount.role === "undefined")
            next(createError(404));
        else
            next();
    }
};