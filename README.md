### test 2 Api Rest
```bash
    npm install Ou yarn install
```

    Le but de ce test est de réaliser une API nodeJS Express d'une gestion d'articles liée à une base de données mysql.
    L'Api devra intégrer deux rôles :
        -  User, qui pourra récupérer tous les articles.
        -  Admin, qui pourra récupérer, modifer, supprimer n'importe quel article et en ajouter un.
     
    Elle devra être composée de plusieurs routes:
        - /articles                             -> Recupère tous les articles avec son auteur
        - /articles/article/:idArticle          -> Recupère un article et son auteur
        - /article/add                          -> Ajoute un article accessible uniquement par un admin
        - /article/update                       -> modifie un article accessible uniquement par un admin
        - /article/delete/:idArticles           -> supprime un ou plusieurs articles accessible uniquement par un admin
        - /login                                -> connexion 
        
    Une personne non connectée ne pourra pas accéder aux articles. Il faudra donc une connexion sécurisée avec JsonWebToken et bcrypt.
        - Compte user
            - mail: user@user.fr
            - mdp: azerty
        - Compte Admin
            - mail: root@root.fr
            - mdp: root
    
    Le token devra être présent dans le header de celle-ci sous la forme suivante:
        - [Authorization] Bearer ${token}
    
    Vous avez des ressources a disposition dans le dossier middleware. Il est composé de 3 middleware:
        - checkToken    -> verifie la présence d'un token dans la requetes
        - checkAtuh     -> verifie la validité du token et place ses information dans req.authData
        - isAdmin       -> verifie si c'est un admin qui essaye d'acceder a la ressource renvoie une 404 si ca ne l'est pas 
   
    Ansi qu'une fonction qui se connecte a la base de donnée dans le fichier helpers/connection.js
    
    
#### Si vous utilisez docker-compose
    Informations de connexion à la base de données 
        - bdd: articles
        - mdp: root
        - user: root
    
    Ports utilisés
        - 8090 -> Phpmyadmin
        - 8080 -> Api
        - 3306 -> mysql
    Pour lancer les différent services -> docker-compose up Ou installer le serveur sql sur votre machine

    Infomartions disponibles dans l'environement du serveur:
        D8_HOST: base de données
        DB_PASSWORD: mot de passe de la base de données
        DB_NAME: nom de la base de donnée
        JWT_PRIVATE_KEY: clef privé jwt
    ```
````    
Vous pouvez ajouter autant de dépendences que vous voulez si cela vous semble nécessaire 
vous pouvez également créer autant de fichiers ou de dossiers qu'il vous semble nécessaire
````

### Base de donnée
*   nom de la base: articles
#### Schema bdd
![Drag Racing](bddScreen.png)