const express = require("express");
const router = express.Router();
const checkHeader = require("../src/middleware/checkHeader");

router.get("/", async (req, res, next) => {
});

router.get("/article/:idArticle", async (req, res, next) => {
});


router.post("/add", async (req, res, next) => {

});

router.put("/update", async (req, res, next) => {
});

module.exports = router;