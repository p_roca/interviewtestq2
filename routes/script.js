const express = require("express");
const router = express.Router();
const mysql = require("promise-mysql");
const faker = require("faker");
const bcrypt = require("bcrypt");


router.get("/", async (req, res, next) => {
    const connectionInfo = {
        host: process.env.DB_HOST || "localhost",
        user: process.env.DB_USER || "root",
        password: process.DB_PASSWORD || "root",
        database: process.env.DB_NAME || "root",
        waitForConnections: true
    };
    let addAccount = `
        INSERT INTO
            account
        SET
            mail = ?,
            firstName = ?,
            lastName = ?,
            password = ?,
            admin = ?;
    `;
    let addArticles = `
        INSERT INTO
            articles
        SET
            title = ?,
            content = ?,
            idAccount = ?
    `;
    try {
        const connection = await mysql.createConnection(connectionInfo);
        const saltRound = 10;
        let hashPassword = await bcrypt.hash("root", saltRound);
        const infoInsert = await connection.query(addAccount, ["root@root.fr", faker.name.firstName(), faker.name.lastName(), hashPassword, 1]);
        const insertId = infoInsert.insertId;
        hashPassword = await bcrypt.hash("azerty", saltRound);
        await connection.query(addAccount, ["user@user.fr", faker.name.findName(), faker.name.lastName(), hashPassword, 0]);
        for (let i = 0; i < 10; i++) {
            await connection.query(addArticles, [faker.lorem.word(), faker.lorem.paragraphs(), insertId]);
        }
        await connection.end();
        res.status(200).send("OK")
    } catch (e) {
        console.error(e);
        res.status(500).send(e)
    }
});

module.exports = router;